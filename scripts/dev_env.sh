export FLASK_APP=manage.py
export FLASK_ENV=development
export DATABASE_URL='postgres://postgres:root@localhost/helmsboot'
export APP_SETTINGS='project.config.DevConfig'
