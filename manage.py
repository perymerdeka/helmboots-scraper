from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from project import create_app, db

# configure app
app = create_app()
migrate = Migrate(app, db)
manager = Manager(app)

# add command
manager.add_command('db', MigrateCommand)

# run app
if __name__=='__main__':
    manager.run()