from project import db

class Records(db.Model):
    __tablename__= 'records'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String, nullable=False)
    handle = db.Column(db.String, nullable=False)
    publish = db.Column(db.DateTime, nullable=True)
    product_id = db.Column(db.String, nullable=False)
    price = db.Column(db.String, nullable=False)
    product_type = db.Column(db.String, nullable=False)
    sku = db.Column(db.String, nullable=False)
    vendor = db.Column(db.String, nullable=False)
    image_url = db.Column(db.String, nullable=False)

    # define name init
    def __init__(self, title, handle,product_id, price ,publish, product_type, sku, vendor, image_url):
        self.title = title
        self.handle = handle
        self.publish =publish
        self.product_id = product_id
        self.price = price
        self.product_type = product_type
        self.sku = sku
        self.vendor = vendor
        self.image_url = image_url

    def __repr__(self):
        return '<title {}'.format(self.title)