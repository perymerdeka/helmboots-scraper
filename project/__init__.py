import os
from flask import Flask
from flask_debugtoolbar import DebugToolbarExtension
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

# inisiate extention
db = SQLAlchemy()
toolbar = DebugToolbarExtension()
migrate = Migrate()

# configure extentsion
def create_app(script_info=None):
    from project.models import models
    from project.scraper.views import scraper_blueprint

    # inisiate app
    app = Flask(__name__, static_url_path='')

    # setting config
    app_settings = os.getenv('APP_SETTINGS')
    app.config.from_object(app_settings)

    # setup extension
    db.init_app(app)
    toolbar.init_app(app)
    migrate.init_app(app, db)

    # register blueprint
    app.register_blueprint(scraper_blueprint)

    # shell context for flask cli
    @app.shell_context_processor
    def ctx():
        return {'app':app, 'db':db}

    return app