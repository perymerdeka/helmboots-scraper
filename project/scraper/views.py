from flask import Blueprint, url_for, render_template, redirect, request
import requests
import json
import pandas as pd
import os

# database config
from project import db
from project.models.models import Records

# headers
headers = {
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'accept-encoding': 'gzip, deflate, br',
    'cache-control': 'max-age=0',
    'cookie': '__cfduid=d83673cf7d88f3d5743df615d2a3c68961601514796; _shopify_y=239022ca-28a9-4e23-b486-f896c1203ace; cart_currency=USD; secure_customer_sig=; _y=239022ca-28a9-4e23-b486-f896c1203ace; _shopify_fs=2020-10-01T01%3A13%3A16Z; _orig_referrer=https%3A%2F%2Fwww.google.com%2F; _landing_page=%2F; _s=0fb9bd47-c203-4dff-9c18-8502ff0eba62; _shopify_s=0fb9bd47-c203-4dff-9c18-8502ff0eba62; _shopify_sa_p=; SL_GWPT_Show_Hide_tmp=1; SL_wptGlobTipTmp=1; shopify_pay_redirect=pending; cart_sig=; _privy_C5A3105DD4E222601F5686EE=%7B%22uuid%22%3A%22f4961ba4-313b-4ca4-b85b-1afc49c7a5fd%22%2C%22cart_value%22%3A0%2C%22variations%22%3A%7B%7D%2C%22country_code%22%3A%22ID%22%2C%22region_code%22%3A%22ID_%22%2C%22postal_code%22%3A%22%22%7D; privy_suppress_1018995=1601514809; _shopify_sa_t=2020-10-01T01%3A37%3A30.601Z; amplitude_id_0d882e02683d5df9d3e5364fb895c64fhelmboots.com=eyJkZXZpY2VJZCI6IjM3YzNiNmEyLThlNmMtNDgxMy1iMTBkLTU5MDI1YWRjNDY3NlIiLCJ1c2VySWQiOiJmZDJkODdlNi1iNjM2LTQ1ZDMtOWExMS1kMTUwNzMzMGE0MzAiLCJvcHRPdXQiOmZhbHNlLCJzZXNzaW9uSWQiOjE2MDE1MTQ4MDQyNTQsImxhc3RFdmVudFRpbWUiOjE2MDE1MTYyNTM4MzgsImV2ZW50SWQiOjgsImlkZW50aWZ5SWQiOjI0LCJzZXF1ZW5jZU51bWJlciI6MzJ9; _shopify_tm=; _shopify_m=persistent; _shopify_tw=',
    'if-none-match': 'cacheable:939c41f2c66d4fd95e26236e1343d466',
    'sec-fetch-dest': 'document',
    'sec-fetch-mode': 'navigate',
    'sec-fetch-site': 'none',
    'sec-fetch-user': '?1',
    'upgrade-insecure-requests': 'upgrade-insecure-requests',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36',
}

# blueprint
scraper_blueprint = Blueprint('scraper', __name__, template_folder='templates')


# helper
def get_detail(limit, page):
    global images
    params = {
        'limit': limit,
        'page': page
    }

    # scraping process

    # define url
    url = 'https://helmboots.com/products.json'
    source = requests.get(url, params=params, headers=headers)
    data = source.json()

    # product list
    product_list = []

    # scraping step
    for item in data['products']:
        title = item['title']
        handle = item['handle']
        publish = item['published_at']
        product_type = item['product_type']
        vendor = item['vendor']

        for image in item['images']:
            try:
                images = image['src']
            except:
                images = 'no image found'
        for variant in item['variants']:
            sku = variant['sku']
            price = variant['price']
            product_id = variant['product_id']

            # save product
            product = {
                'title': title,
                'handle': handle,
                'publish': publish,
                'product_id': product_id,
                'price': price,
                'product type': product_type,
                'sku': sku,
                'vendor': vendor,
                'image url': images
            }

            product_list.append(product)
        return product_list


# creating route
@scraper_blueprint.route('/')
def home():
    records = Records.query.all()
    return render_template('index.html', records=records)


@scraper_blueprint.route('/get-data')
def get_data_product():
    url = 'https://helmboots.com/products.json/'
    source = requests.get(url)
    data = source.json()

    # save to json file
    try:
        # creating directory
        os.makedirs('results')
    except FileExistsError:
        pass
    with open('results/source_data.json', 'w') as outfile:
        json.dump(data, outfile)
    print('data saved successfully')

    return redirect(url_for('scraper.home'))


# route to view data
@scraper_blueprint.route('/view-data', methods=['GET', 'POST'])
def view_data():
    global images

    if request.method == 'POST':
        list_product = get_detail(limit=request.form['limitvalue'], page=request.form['pagedisplay'])
        for value in list_product:
            title = value['title']
            handle = value['handle']
            publish = value['publish']
            product_id = value['product_id']
            price = value['price']
            product_type = value['product type']
            sku = value['sku']
            vendor = value['vendor']
            image_url = value['image url']

            # save to database
            new_records = Records(title=title, handle=handle, product_id=product_id, price=price, publish=publish,
                                  product_type=product_type, sku=sku, vendor=vendor, image_url=image_url)
            db.session.add(new_records)
            db.session.commit()

        print('data saved to database')

        # save to csv
        df = pd.DataFrame(list_product)
        df.to_csv('./results/result.csv')
        print('data saved to csv success')
        return redirect(url_for('scraper.home'))

    return render_template('view-data.html')
