helmboots webscraper website download free @ gitlab.com

### Running Development

1. Create Database with this command

```bash
CREATE DATABASE helmboots_scraper;
GRANT ALL PRIVILEGES ON database helmboots_scraper to helmboots-admin;
ALTER USER helmboots-admin SUPERUSER;
```

2. Setup VirtualEnv 

running this command

```bash
python -m venv venv
pip install -r requirements.txt
source ./script/run.sh
```

DEMO APPS URL: [https://helmboot-scraper.herokuapp.com/](https://helmboot-scraper.herokuapp.com/)